#!/bin/bash
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. ${SCRIPTPATH}/.common
basedir=$(pwd)
#set -x

usage() { echo "Usage: $0  archive-names
  archive-names is a list of archive-files
  Optional parameters:"
}

if [[ $# -lt 1 ]]; then
  usage
fi

for archive in $@; do
  echo $archive
  mkdir /tmp/removeConfidentalData
  cd /tmp/removeConfidentalData
  tar -xzf ${basedir}/${archive} 
  REPO_DIR=$(sed '1q;d' ${META_DATA})
  sed -i 's/name="gitlabAuthToken" value=".*"/name="gitlabAuthToken" value=""/g' "${REPO_DIR}/.idea/PipelineViewerConfig.xml"
  tar -czf $archive "$REPO_DIR"
  cd $basedir
  mv /tmp/removeConfidentalData/$archive .
  rm -rf /tmp/removeConfidentalData
done
