#!/bin/bash
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. ${SCRIPTPATH}/.common
basedir=$(pwd)
touched=0
## The find must be stand at the end see https://github.com/koalaman/shellcheck/wiki/SC2031
while IFS= read -r -d '' file; do
    repodir=$(dirname "$file")
    cd "${repodir}"
    gitstate=$(checkGitState)
    pushstate=$(checkGitUnPushed)
    if [[ -n ${gitstate} || -n ${pushstate} ]]; then
      echo "### ${repodir} ###"
      checkGitState
      checkGitUnPushed
      touched=$((touched+1))
    fi
    cd "$basedir"
done < <(find . -type d -name '.git' -print0)
exit $touched
