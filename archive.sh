#!/bin/bash
set -eu
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. ${SCRIPTPATH}/.common
basedir=$(pwd)
backupdir=~/sandbox_backup
remoteBackupDir=~/OneDrive/backup/
mkdir -p ${backupdir} 2> /dev/null || true
find . -type d -name '.git' -print0 | 
while IFS= read -r -d '' file; do
    repodir=$(dirname "$file")
    slugname=${repodir//[ \/]/_}
    archivename="${backupdir}/${slugname:2}${ARCHIVE_SUFFIX}"
    if [ -e $archivename ]; then
      mv $archivename $archivename.bak
    fi
    # find url
    cd "$repodir"
    repourl=$(git remote -v |head -1|cut  -f 2 |cut -d ' ' -f1)
    cd "$basedir"
    if [[ -z $repourl  ]]; then
      echo "$repodir has no remote. No archive created"
    else 
      echo "$repodir" > ${META_DATA}
      echo "$repourl" >> ${META_DATA}
      localTarName=tar-archive
      if [[ -d "$repodir/.idea" ]]; then
        tar -cf ${localTarName} --exclude=*.sample ${META_DATA} "$repodir/.git/hooks" "$repodir/.git/config" "$repodir/.idea"
      else 
        tar -cf ${localTarName} --exclude=*.sample ${META_DATA} "$repodir/.git/hooks" "$repodir/.git/config"
      fi
      for addfile in .envrc .sdkmanrc; do
        if [[ -f "$repodir/$addfile" ]]; then
          tar -uf ${localTarName} "$repodir/$addfile"
        fi
      done
      rm ${META_DATA}
      gzip ${localTarName}
      mv ${localTarName}.gz $archivename
    fi
done
cd ${backupdir}
tar -cf ${remoteBackupDir}/sandboxBackup.tar *
cd ${basedir}
