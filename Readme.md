# Git - Multi-Repo-Helper
On large projekt is often cumbersome to handle many repositories.
In this repository you find bash-scripts for the following tasks:
- archive.sh - archive a repo, so that you can easily get the same structure on other computer
- restore.sh - clone the repo from archive at the given directory and restore the config.<br>
  You can set the option `-c`, `-h` or `-i` to restore config ,hooks or .idea (which is not really git-specific).
- status.sh - get the status overview of the repos.

## Use-Cases:
- Move repos from one computer to another

## Limitations
- The url is always the first remote
- Restore simple clone the repo, so a bare-repo change his state and the config file could be wrong in this case
