#!/bin/bash
set -eu
SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")
. ${SCRIPTPATH}/.common

usage() { echo "Usage: $0 [-c][-h] archive-names
  archive-names is a list of archive-files
  Optional parameters:
  -c to restore the config
  -h to restore the hooks
  -i idea folder" 1>&2; exit 1; 
}

extract="${META_DATA}"
while getopts :hci opt
do
   case $opt in
       h) extract="${extract} */.git/hooks*";;
       c) extract="${extract} */.git/config";;
       i) extract="${extract} */.idea*"
   esac
done
shift $((OPTIND-1))
if [[ $# -lt 1 ]]; then
  usage
fi
for archive in $@; do
  echo $archive
  tar -xzf ${archive} ${META_DATA}
  REPO_DIR=$(sed '1q;d' ${META_DATA})
  REPO_URL=$(sed '2q;d' ${META_DATA})
  git clone $REPO_URL "${REPO_DIR}"
  set -o noglob
  tar --wildcards -xzvf ${archive} ${extract}
  rm ${META_DATA}
done